import {
  Card,
  Button,
  Col,
  Avatar,
  Row,
  Menu,
  Upload,
  message,
  Collapse,
  Icon,
  Select,
  Modal,
  Spin,
  Input,
  Pagination,
  Dropdown,
} from 'antd';
import { router } from 'umi';
import { connect } from 'dva';
import React, { useState, useEffect, useCallback } from 'react';
import SectionForms from './SectionForms';
import PermissionsTable from './permissionsTable';
import {
  Actions,
  GET_HISTORY,
  GET_TEMPLATES_DETAILS,
  GET_COMMENTS,
} from '@/actions/userProfile';
import UserDataSection from './sections/userDataSection';
import { Dispatch, AnyAction } from 'redux';
import {
  createUserProfileComments,
  addUserProfileHistoryAsync,
  getUserProfileById,
} from '@/services/user';
import { ConnectState } from '@/models/connect';
import moment from 'moment';
import style from './style.less';
import HistoryList from './components/renderHistoryList';
import HistoryListSummary from './components/renderHistoryList/renderHistorySummary';
import { generatePayload } from './profileUtils/updateConstants';
import { getDiff } from './profileUtils/getObjectDiff';
import { PaginationConfig, SorterResult } from 'antd/lib/table';
import DataTable from '@/components/DataTable';
import { deleteUserProfileComment } from '@/services/user';
import InputTextFilter from '@/components/InputTextFilter';
import DateRangeFilter from '@/components/DateRangeFilter';

interface Props {
  dispatch: Dispatch<Actions | AnyAction>;
  selectedUser: any;
  permissions: any;
  userHistory: any;
  userComments: any;
  templates: any;
  userGUID: string;
  loggedInUser: string;
  touch: any;
  untouch: any;
}
const initialValPermissions: any = ['permissions'];
const initialValComments: any = ['comments'];

const menuItems: any = [
  { label: 'General Information', value: 'GeneralInformation' },
  { label: 'Property', value: 'Property' },
  // { label: 'Contact Information', value: 'ContactInformation' },
  { label: 'Network', value: 'Network' },
  { label: 'Coding', value: 'Coding' },
  { label: 'Comping', value: 'Comping' },
  { label: 'Messaging', value: 'Messaging' },
];

const testData: any = {
  MemberName: 'Paul Rose',
  MemberID: '112804',
  MemberEmail: 'paul.rose@hrsc.com',
};

const defaultPageSize = 10;

const UserProfileView: React.FC<Props> = (props) => {
  const { Panel } = Collapse;
  const {
    dispatch,
    selectedUser,
    permissions,
    userHistory,
    templates,
    userGUID,
    userComments,
    loggedInUser,
    touch,
    untouch,
  } = props;
  const [isAddingComment, setAddComment] = useState(false);
  const [isShowHistory, setShowHistory] = useState(false);
  const [isShowUpload, setShowUpload] = useState(false);
  const [isEditMode, setEditMode] = useState(false);
  const [isShowTemplateSelector, setShowTemplateSelector] = useState(false);
  const [isSaving, setSaving] = useState(false);
  const [isConfirm, setConfirm] = useState(false);
  const [isConfirmed, setConfirmed] = useState(false);
  const [viewingSection, setViewSection] = useState(['GeneralInformation']);
  const [isCollapseUserPermission, setCollapseUserPermission] = useState(false);
  const [
    collapseUserPermissionActiveKey,
    setCollapseUserPermissionActiveKey,
  ] = useState([]);
  const [isCollapseUserComment, setCollapseUserComment] = useState(false);
  const [
    collapseUserCommentActiveKey,
    setCollapseUserCommentActiveKey,
  ] = useState([]);
  const [userId, updateUserId] = useState<string>(userGUID);
  const [comment, setComment] = useState('');
  const [commentCounter, setCommentCounter] = useState(0);
  const [newVal, updateNewVal] = useState([]);
  const [isLoading, updateIsLoading] = useState(true);
  const [primaryData, updatePrimaryData] = useState({});
  const [historySearchString, updateHistorySearchString] = useState('');
  const [templatesVar, updateTemplates] = useState([]);
  const [isLoadingHistory, updateisLoadingHistory] = useState(true);
  const [changedItems, updateChangedItems] = useState([]);
  const [isAddCommentBtn, setAddCommentBtn] = useState(false);
  const [currentUserName, setcurrentUserName] = useState('');
  const [isDeleteUserLoading, setIsDeleteUserLoading] = useState(false);
  const [currentCommentSort, setCurrentCommentSort] = useState('');
  const [emailMessage, setEmailMessage] = useState('');
  const [empIdMessage, setEmpIdMessage] = useState('');
  const [emailValid, setEmailValid] = useState(false);
  const [empIdValid, setEmpIdValid] = useState(false);
  const [teamNameValid, setTeamNamedValid] = useState(false);
  const [userCommentsName, setUserCommentsName] = useState(
    props.userComments.data
  );
  const [commentPagination, setpagination] = useState<PaginationConfig>({
    defaultCurrent: 1,
    position: 'bottom',
    pageSize: defaultPageSize,
  });
  const immutable = selectedUser;

  const getCommentsPayload = {
    args: {
      id: userId,
      userId: userId,
      pageNo: 1,
      pageSize: defaultPageSize,
      search: '',
      sort: currentCommentSort,
    },
  };

  const getValidationMessage = (emailMessage: string, empMessage: string) => {
    setEmailMessage(emailMessage);
    setEmpIdMessage(empMessage);
  };

  // Triggers the fetch-history action of the selected user
  const triggerHistoryList = async () => {
    updateisLoadingHistory(true);
    dispatch(
      Actions.fetchProfileHistoryAction({
        userID: userGUID,
        searchString: historySearchString,
      })
    ).then(() => {
      updateisLoadingHistory(false);
    });
  };

  useEffect(() => {
    triggerHistoryList();
  }, [historySearchString]);

  const onChangeUserPermissionBlock = (v: any) => {
    if (!isCollapseUserPermission) setCollapseUserPermissionActiveKey([]);
    setCollapseUserPermissionActiveKey(v);
    setCollapseUserPermission(v && v.length > 0);
  };

  const onChangeUserCommentBlock = (v: any) => {
    if (!isCollapseUserComment) setCollapseUserCommentActiveKey([]);
    setCollapseUserCommentActiveKey(v);
    setCollapseUserComment(v && v.length > 0);
  };

  const reEvaluateInput = () => {
    if (newVal['comping']['abilityToIssueComp'] == 0) {
      newVal['comping'].compAmountPerDay = '';
      newVal['comping'].compAmountPerDayNoLimit = false;
      newVal['comping'].compLimitsPerTransaction = '';
      newVal['comping'].compPerTransactionNoLimit = false;
    } else if (newVal['comping']['abilityToIssueComp'] == 1) {
      if (newVal['comping'].compAmountPerDayNoLimit) {
        newVal['comping'].compAmountPerDay = '';
      }
      if (newVal['comping'].compPerTransactionNoLimit) {
        newVal['comping'].compLimitsPerTransaction = '';
      }
    }

    if (newVal['messaging']['abilityToGroupMessage'] == 0) {
      newVal['messaging'].groupMessageLimits = '';
      newVal['messaging'].groupMessageMaxLimit = false;
    } else if (newVal['messaging']['abilityToGroupMessage'] == 1) {
      if (newVal['messaging'].groupMessageMaxLimit) {
        newVal['messaging'].groupMessageLimits = '';
      }
    }
  };

  const AccessUI = () => {
    let toDisplay = [],
      titleAccess = '';

    touch.forEach((touchValue) => {
      let filterUntouch = touch.filter(
        (untouchValue) => touchValue.memberId === untouchValue.memberId
      )[0];

      if (filterUntouch.access) {
        titleAccess = 'HAS ACCESS';

        if (filterUntouch.quickSelect.allAccess) {
          titleAccess = 'ALL ACCESS';
        }

        if (filterUntouch.quickSelect.restrictedAccess) {
          titleAccess = 'RESTRICTED ACCESS';
        }
      } else {
        titleAccess = 'NO ACCESS';
      }

      toDisplay.push({ title: titleAccess, value: touchValue.permission });
    });
    return toDisplay;
  };
  const onTriggerAction = () => {
    if (isEditMode) {
      const data = newVal.general;
      if (
        data.teamMemberID != '' &&
        data.workEmail != '' &&
        data.teamMemberName != ''
      ) {
        reEvaluateInput();
        setSaving(true);
        updateIsLoading(true);

        let obj = { ...newVal, general: { ...newVal.general, ...primaryData } };
        // temporary approach: START
        getUserProfileById(userId).then((e) => {
          const templateDiff = AccessUI();
          const itemsUpdated = getDiff(e.data, obj, templateDiff);
          const payload = generatePayload.UPDATE(
            loggedInUser.userName,
            userId,
            itemsUpdated
          );
          addUserProfileHistoryAsync({ payload });
        });
        // temporary approach: END
        dispatch(
          Actions.updateUserProfile({
            id: userId,
            body: { ...obj, Permissions: permissions },
          })
        ).then((e) => {
          dispatch(Actions.getUserProfile(userId)).then((e) => {
            triggerHistoryList();
            updateIsLoading(false);
            message.success('User has been successfully updated.');
          });
        });
        setSaving(false);
      } else {
        message.error('All entry must be fill.');
      }
    }
    setEditMode(!isEditMode);
    setSaving(false);
  };
  useEffect(() => {}, [userHistory]);
  useEffect(() => {
    updateIsLoading(false);
    // initialDispatch();
    updateNewVal(selectedUser);
    updateTemplates(templates);
    dispatch(
      Actions.fetchProfileCommentsAction({
        args: {
          id: userId,
          userId: userId,
          pageNo: 1,
          pageSize: defaultPageSize,
          search: '',
          sort: currentCommentSort,
        },
      })
    );
  }, []);

  const handleDeleteUser = () => {
    const payload = userId;
    setIsDeleteUserLoading(true);

    dispatch(Actions.deleteUserProfile(payload)).then(() => {
      setIsDeleteUserLoading(false);
      setConfirmed(true);
    });
  };

  useEffect(() => {
    setpagination({
      ...commentPagination,
      total: userComments.paging.totalCount,
    });
  }, [userComments]);

  const updatePrimary = (e) => {
    updatePrimaryData({ ...primaryData, [e.target.name]: e.target.value });
  };

  const updateData = useCallback((val, e = null) => {
    updateNewVal(val);
  });

  const changeTemplate = (id: number | null | undefined) => {
    dispatch(Actions.fetchTemplateDetails(id));
  };

  const handleDelete = (obj: any) => {
    const payload = {
      id: obj.id.toString(),
    };
    deleteUserProfileComment(userId, payload)
      .then((res) => {
        if (res.result) {
          dispatch(
            Actions.fetchProfileCommentsAction({
              args: {
                id: userId,
                userId: userId,
                pageNo: 1,
                pageSize: 10,
                search: '',
                sort: currentCommentSort,
              },
            })
          );
        }
      })
      .catch((err) => console.log(err));
  };

  const [mColumns, setColumns] = useState([
    {
      title: 'Actions',
      key: 'actions',
      width: 110,
      sorter: false,
      render: (text, record) => {
        return (
          <Dropdown
            placement="bottomLeft"
            overlay={() => menu(text)}
            trigger={['hover']}
          >
            <Icon
              type="ellipsis"
              style={{ fontSize: '20px', color: '#000000', margin: '0px 24px' }}
            />
          </Dropdown>
        );
      },
    },
    {
      title: 'Date Entered',
      key: 'CreatedDate',
      dataIndex: 'CreatedDate',
      render: (item: any) => (
        <span>{item ? moment(item).format('MMM DD, YYYY') : ''}</span>
      ),
      sorter: true,
      width: 200,
      filterDropdown: ({
        setSelectedKeys,
        selectedKeys,
        confirm,
        clearFilters,
        visible,
      }: any) => (
        <DateRangeFilter
          value={selectedKeys}
          visible={visible}
          onConfirm={(filters: Array<string>) => {
            setSelectedKeys(filters);
            confirm();
          }}
          onReset={() => {
            setSelectedKeys([]);
            clearFilters();
          }}
        />
      ),
    },
    {
      title: 'Logged By',
      key: 'CreatedByName',
      dataIndex: 'CreatedByName',
      sorter: true,
      width: 250,
      filterDropdown: ({
        setSelectedKeys,
        selectedKeys,
        confirm,
        clearFilters,
        visible,
      }: any) => (
        <InputTextFilter
          type="text"
          value={selectedKeys[0]}
          placeholder="Guest Name"
          visible={visible}
          onConfirm={(filter: string) => {
            setSelectedKeys([filter]);
            confirm();
          }}
          onReset={() => {
            setSelectedKeys([]);
            clearFilters();
          }}
        />
      ),
    },
    {
      title: 'Note',
      key: 'Note',
      dataIndex: 'Note',
    },
  ]);

  const handleTableChange = (pagination, filters, sorter, extra) => {
    setpagination({
      ...pagination,
    });

    // asasdasdas
    let sortColumnKey = undefined;
    let submittedDateFrom = undefined;
    let submittedDateTo = undefined;
    let CreatedByName = undefined;

    if (sorter.columnKey) {
      sortColumnKey =
        sorter.order === 'ascend'
          ? `${sorter.columnKey}Asc`
          : `${sorter.columnKey}Desc`;
    }

    if (filters) {
      if (filters.CreatedDate && filters.CreatedDate.length === 2) {
        submittedDateFrom = filters.CreatedDate[0];
        submittedDateTo = filters.CreatedDate[1];
      }

      if (filters.CreatedByName && filters.CreatedByName.length > 0) {
        CreatedByName = filters.CreatedByName[0]; //pick the first element only
      }
    }

    setCurrentCommentSort(sortColumnKey);
    setUserCommentsName(CreatedByName);
    dispatch(
      Actions.fetchProfileCommentsAction({
        args: {
          id: userId,
          userId: userId,
          pageNo: pagination.current,
          pageSize: 10,
          createdByName: CreatedByName,
          createdDateFrom: submittedDateFrom,
          createdDateTo: submittedDateTo,
          search: '',
          sort: sortColumnKey,
        },
      })
    );
  };

  const menu = (id: any) => {
    return (
      <Menu>
        <Menu.Item>
          <a onClick={() => handleDelete(id)}>Delete Comment</a>
        </Menu.Item>
      </Menu>
    );
  };

  const cardTitleUI = () => (
    <div style={{ textAlign: 'center' }}>
      <Button
        type="primary"
        onClick={() => onTriggerAction()}
        disabled={isLoading || !emailValid || !empIdValid || !teamNameValid}
        className="btnUserProfileEdit stlBtnHeader"
        style={{ float: 'right' }}
      >
        {
          <Spin spinning={isSaving} tip="Saving">
            {isEditMode ? 'Save' : 'Edit'}
          </Spin>
        }
      </Button>
      <span>OPTX USER PROFILE</span>
    </div>
  );

  const menuListItems = () => {
    let lstMenuItems: any = [];
    menuItems.forEach((element: any) => {
      lstMenuItems.push(
        <Menu.Item key={element.value}>{element.label}</Menu.Item>
      );
    });

    return lstMenuItems;
  };

  const onMenuClickChange = (
    item: any,
    key: any,
    keyPath: any,
    domEvent: any
  ) => {
    let section: any = [];
    section.push(item.key);
    setViewSection(section);
  };

  const mdlConfirm = () => {
    return (
      <Modal
        title="DELETE USER"
        onCancel={() => setConfirm(false)}
        visible={isConfirm}
        footer={null}
      >
        <p style={{ textAlign: 'center' }}>
          {!isConfirmed ? (
            <span>
              Are you sure you would like to delete the user{' '}
              <b>{selectedUser ? selectedUser.general.teamMemberName : ''}?</b>
            </span>
          ) : (
            <span>
              <b>{selectedUser ? selectedUser.general.teamMemberName : ''}</b>{' '}
              has been deleted
            </span>
          )}
        </p>
        <p style={{ textAlign: 'center' }}>
          {!isConfirmed ? (
            <span>
              <Button
                style={{ width: 100, borderRadius: 0, marginRight: 10 }}
                onClick={() => setConfirm(false)}
              >
                Cancel
              </Button>
              <Button
                style={{ width: 100, borderRadius: 0 }}
                type="primary"
                loading={isDeleteUserLoading}
                onClick={() => handleDeleteUser()}
              >
                Yes
              </Button>
            </span>
          ) : (
            <Button
              onClick={() => router.goBack()}
              style={{ width: 100, borderRadius: 0 }}
              type="primary"
            >
              Done
            </Button>
          )}
        </p>
      </Modal>
    );
  };

  const mdlHistory = () => {
    return (
      <Modal
        title="USER HISTORY"
        onCancel={() => setShowHistory(false)}
        visible={isShowHistory}
        footer={null}
      >
        <div>
          <div
            className={style.userHistoryContainer}
            style={{ height: '500px', overflow: 'auto' }}
          >
            {userHistory.data && userHistory.data.length === 0 ? null : (
              <HistoryList
                isLoading={isLoadingHistory}
                historyList={userHistory.data}
              ></HistoryList>
            )}
          </div>
        </div>
      </Modal>
    );
  };

  const uploadProps: any = {
    name: 'file',
    multiple: true,
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    onChange(info) {
      const { status } = info.file;
      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (status === 'done') {
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  const mdlUpload = () => {
    return (
      <Modal
        title="UPDATE USER PHOTO"
        onCancel={() => setShowUpload(false)}
        visible={isShowUpload}
        footer={null}
      >
        <Row type="flex" align="middle" justify="center">
          <Col span={12}>
            <Upload.Dragger {...uploadProps}>
              <Row type="flex" align="middle" justify="center">
                <Col span={6}>
                  <p className="ant-upload-drag-icon">
                    <Icon type="upload" style={{ color: '#333' }} />
                  </p>
                </Col>
                <Col span={12}>
                  <p className="ant-upload-text">Drag & Drop Here</p>
                  <p className="ant-upload-text">or</p>
                  <p className="ant-upload-hint">
                    <u>Browse Computer</u>
                  </p>
                </Col>
              </Row>
            </Upload.Dragger>
          </Col>
        </Row>
        <p style={{ textAlign: 'right', marginTop: 10 }}>
          <Button
            onClick={() => setShowUpload(false)}
            style={{ width: 100, borderRadius: 0, marginRight: 10 }}
          >
            Cancel
          </Button>
          <Button
            onClick={() => setShowUpload(false)}
            style={{ width: 100, borderRadius: 0 }}
            type="primary"
          >
            Save
          </Button>
        </p>
      </Modal>
    );
  };

  const mdlSelectTemplate = () => {
    return (
      <Modal
        title="SELECT TEMPLATE"
        onCancel={() => setShowTemplateSelector(false)}
        visible={isShowTemplateSelector}
        footer={null}
      >
        <Row type="flex" align="middle" justify="center">
          <Col span={6}>Pick Template</Col>
          <Col span={12}>
            <Select
              defaultValue="Template"
              onSelect={changeTemplate}
              style={{ width: '100%' }}
            >
              {templatesVar.data && templatesVar.length === 0
                ? null
                : templatesVar.map((item, id) => {
                    return (
                      <Select.Option key={id} value={item.id}>
                        {item.template}
                      </Select.Option>
                    );
                  })}
            </Select>
          </Col>
        </Row>
        <p style={{ textAlign: 'right', marginTop: 20 }}>
          <Button
            onClick={() => setShowTemplateSelector(false)}
            style={{ width: 100, borderRadius: 0, marginRight: 10 }}
          >
            Cancel
          </Button>
          <Button
            onClick={() => setShowTemplateSelector(false)}
            style={{ width: 100, borderRadius: 0 }}
            type="primary"
          >
            Use
          </Button>
        </p>
      </Modal>
    );
  };

  const genExtra = () => (
    <Button
      size="small"
      style={{ borderRadius: 0 }}
      onClick={() => {
        setAddComment(!isAddingComment);
      }}
    >
      <Icon type="plus" />
    </Button>
  );

  const onCommentChange = (v: any) => {
    setComment(v.target.value);
    setCommentCounter(v.target.value ? v.target.value.length : 0);
  };

  const [commentDataLoading, setCommentDataLoading] = useState(false); //loading

  const onSaveComment = (comment: any) => {
    setCommentDataLoading(true);
    // let setOfComments: any = commentData;
    let newComment: any = {
      id: userId,
      forUser: userId,
      notes: comment,
    };

    createUserProfileComments(newComment).then((res) => {
      if (res.data.result) {
        dispatch(Actions.fetchProfileCommentsAction(getCommentsPayload));
        setCommentDataLoading(false);
        setAddComment(false);
        setComment('');
      }
    });
  };

  const mdlAddUserComment = () => {
    return (
      <Modal
        title="ADD USER  COMMENT"
        visible={isAddingComment}
        onCancel={() => setAddComment(false)}
        footer={null}
      >
        <Input.TextArea
          placeholder="Comment content goes here"
          value={comment}
          rows={4}
          onChange={onCommentChange}
          maxLength={150}
        />
        <p style={{ color: '#aeaeae' }}>{commentCounter} of 150</p>
        <p style={{ textAlign: 'right' }}>
          <Button
            onClick={() => setAddComment(false)}
            style={{ width: 100, borderRadius: 0, marginRight: 10 }}
          >
            Cancel
          </Button>
          <Button
            onClick={() => onSaveComment(comment)}
            style={{ width: 100, borderRadius: 0 }}
            type="primary"
            loading={commentDataLoading}
          >
            Save
          </Button>
        </p>
      </Modal>
    );
  };

  return (
    <Spin spinning={isLoading} tip="Loading">
      <div className="profileViewUI">
        {mdlConfirm()}
        {mdlHistory()}
        {mdlUpload()}
        {mdlSelectTemplate()}

        <Card className="profileViewUICard" title={cardTitleUI()}>
          <Row type="flex" justify="center" align="middle">
            <Col xs={6} sm={6} md={6} lg={6} className="userDetailStl">
              <Avatar size={109} onClick={() => setShowUpload(true)} />
              <UserDataSection
                name="teamMemberName"
                dispatch={dispatch}
                updatePrimary={updatePrimary}
                isEditMode={isEditMode}
                value={
                  props.selectedUser
                    ? props.selectedUser.general.teamMemberName
                    : ''
                }
                label="TEAM MEMBER NAME"
                getEmailResult={(result) => setEmailValid(result)}
                getEmpIdResult={(result) => setEmpIdValid(result)}
                getTeamNameResult={(result) => setTeamNamedValid(result)}
                getMessage={getValidationMessage}
              ></UserDataSection>
            </Col>
            <Col xs={4} sm={4} md={4} lg={4}>
              <UserDataSection
                name="teamMemberId"
                dispatch={dispatch}
                updatePrimary={updatePrimary}
                isEditMode={isEditMode}
                value={
                  props.selectedUser
                    ? props.selectedUser.general.teamMemberID
                    : ''
                }
                label="TEAM MEMBER ID"
                getEmailResult={(result) => setEmailValid(result)}
                getEmpIdResult={(result) => setEmpIdValid(result)}
                getTeamNameResult={(result) => setTeamNamedValid(result)}
                getMessage={getValidationMessage}
              ></UserDataSection>
            </Col>
            <Col xs={4} sm={4} md={4} lg={4}>
              <UserDataSection
                name="workEmail"
                dispatch={dispatch}
                updatePrimary={updatePrimary}
                isEditMode={isEditMode}
                value={
                  props.selectedUser ? props.selectedUser.general.workEmail : ''
                }
                label="TEAM MEMBER EMAIL"
                getEmailResult={(result) => setEmailValid(result)}
                getEmpIdResult={(result) => setEmpIdValid(result)}
                getTeamNameResult={(result) => setTeamNamedValid(result)}
                getMessage={getValidationMessage}
              ></UserDataSection>
            </Col>
            <Col xs={4} sm={4} md={4} lg={4}>
              <div
                style={{
                  color: 'red',
                  paddingBottom: '10px',
                  fontFamily: 'Poppins',
                  fontStyle: 'normal',
                  fontWeight: '500',
                  fontSize: '12px',
                  lineHeight: '24px',
                }}
              >
                <span> {empIdMessage} </span>
                <span> {emailMessage} </span>
              </div>
            </Col>
            <Col span={24} className="sectionDistanceStl">
              <Row type="flex" justify="center">
                <Col xs={6} sm={6} md={6} lg={6}>
                  <Menu
                    style={{ width: 256 }}
                    openKeys={viewingSection}
                    defaultSelectedKeys={viewingSection}
                    mode="inline"
                    onClick={onMenuClickChange}
                  >
                    <Menu.ItemGroup
                      className="userProfileMenuLblStl"
                      title="SETTINGS"
                    >
                      {menuListItems()}
                    </Menu.ItemGroup>
                  </Menu>
                </Col>
                <Col xs={18} sm={18} md={18} lg={18}>
                  <SectionForms
                    frmVal={selectedUser}
                    updateData={updateData}
                    editMode={isEditMode}
                    saving={isSaving}
                    section={viewingSection}
                  />
                </Col>
              </Row>
            </Col>
            <Col span={24} className="sectionDistanceStl">
              <div style={{ textAlign: 'right' }}>
                <Select
                  defaultValue="Template"
                  onSelect={() => setShowTemplateSelector(true)}
                  style={{ width: 150 }}
                >
                  <Select.Option value="Template">Templates</Select.Option>
                </Select>
              </div>
            </Col>
            <Col span={24} className="sectionDistanceStl">
              <Collapse
                className="collapseCustomUserProfile"
                activeKey={collapseUserPermissionActiveKey}
                onChange={onChangeUserPermissionBlock}
              >
                <Panel
                  showArrow={false}
                  header="USER PERMISSIONS"
                  key="permissions"
                  className="panelStlUserProfile"
                >
                  <PermissionsTable isEditMode={isEditMode} />
                </Panel>
                <div
                  className="collapseIconStl"
                  onClick={() =>
                    onChangeUserPermissionBlock(
                      collapseUserPermissionActiveKey ? '' : 'permissions'
                    )
                  }
                >
                  <Icon type={isCollapseUserPermission ? 'up' : 'down'} />
                </div>
              </Collapse>
            </Col>
            <Col span={24} className="sectionDistanceStl">
              {mdlAddUserComment()}
              <span
                style={{ position: 'absolute', right: 10, top: 5, zIndex: 1 }}
              >
                {isEditMode ? genExtra() : null}
              </span>
              <Collapse
                className="collapseCustomUserProfile"
                activeKey={collapseUserCommentActiveKey}
                onChange={onChangeUserCommentBlock}
              >
                <Panel
                  showArrow={false}
                  header="USER COMMENTS"
                  key="comments"
                  className="panelStlUserProfile"
                >
                  <DataTable
                    columns={mColumns}
                    loading={props.commentsLoading}
                    dataSource={props.userComments.data}
                    pagination={commentPagination}
                    onChange={handleTableChange}
                  />
                </Panel>
                <div
                  className="collapseIconStl"
                  onClick={() =>
                    onChangeUserCommentBlock(
                      collapseUserCommentActiveKey ? '' : 'comments'
                    )
                  }
                >
                  <Icon type={isCollapseUserComment ? 'up' : 'down'} />
                </div>
              </Collapse>
            </Col>
            <Col span={24} className="sectionDistanceStl">
              <HistoryListSummary
                isLoading={isLoadingHistory}
                historyList={userHistory.data}
              ></HistoryListSummary>
              <p style={{ textAlign: 'right' }}>
                <Button
                  type="primary"
                  onClick={() => setShowHistory(true)}
                  className="btnUserProfileEdit btnUserProfileHistory"
                >
                  History
                </Button>
                <Button
                  onClick={() => setConfirm(true)}
                  type="primary"
                  className="btnUserProfileEdit btnUserProfileDelete"
                >
                  Delete User
                </Button>
              </p>
            </Col>
          </Row>
        </Card>
      </div>
    </Spin>
  );
};

// export default UserProfileView;
const mapStateToProps = ({ userProfile, loading, user }: ConnectState) => {
  return {
    selectedUser: userProfile.selectedUser,
    permissions: userProfile.active.permissions,
    userHistory: userProfile.userProfileHistory,
    loggedInUser: user.currentUser,
    templates: userProfile.templates,
    loading: loading.effects[GET_TEMPLATES_DETAILS],
    commentsLoading: loading.effects[GET_COMMENTS],
    userComments: userProfile.userProfileComments,
    touch: userProfile.touch,
    untouch: userProfile.untouch,
  };
};

export default connect(mapStateToProps)(UserProfileView);
