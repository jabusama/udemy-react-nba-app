import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Layout from './hoc/Layouts/layout';
import routeLinks from './routes';

function App() {
  return (
    <Router>
      <div className="App">
        <Layout>
          <Switch>
            { routeLinks.map( link => <Route {...link} />)}
          </Switch>    
        </Layout>
      </div>
    </Router>
  );
}

export default App;
