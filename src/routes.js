import Home from './components/Home/home';
import About from './components/About/about';

const routeLinks = [
  {
    key: 'home',
    Label: 'Home',
    icon: 'fas fa-home',
    exact: true,
    path: '/',
    component: Home
  },
  {
    key: 'about',
    Label: 'Abour',
    icon: 'fas fa-home',
    path: '/about',
    component: About
  }
]

export default routeLinks;