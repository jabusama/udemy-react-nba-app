import React from 'react'
import './layout.css'

import Header from '../../components/Header/header'

export default function Layout({ children }) {
  return (
    <div>
      <Header />
      { children }
      layout
    </div>
  )
}
